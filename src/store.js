import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
title:"my sweet project",
links: [
  'http://google.com',
  'http://coursetro.com',
  'http://youtube.com'
]
  },
  mutations: {

  },
  actions: {

  }
})
