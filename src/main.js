import Vue from 'vue'
import App from './App.vue'
import router from './router'
import VueRouter from 'vue-router';
import store from './store'

Vue.config.productionTip = false
import vuetify from '@/plugins/vuetify'
import 'vuetify/dist/vuetify.min.css'
import BootstrapVue from 'bootstrap-vue/dist/bootstrap-vue.esm';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import '@mdi/font/css/materialdesignicons.css';
import vueHeadful from 'vue-headful';
import Meta from 'vue-meta';

Vue.use(Meta);

const vuetifyOptions = {}
Vue.config.devtools=false
Vue.config.productionTip=false

Vue.use(vueHeadful, {
  // key: 'myMetaTags', // custom key for component option (optional)
  component: true
})
Vue.use(VueRouter);
Vue.use(vuetify)
Vue.use(require('vue-moment'));
Vue.use(BootstrapVue)

new Vue({
  router,
  store,
  vuetify,
  // vuetify: new Vuetify(vuetifyOptions),
  render: h => h(App)
}).$mount('#app')
