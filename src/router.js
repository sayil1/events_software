import Vue from 'vue'
import Router from 'vue-router'
import Home from '../src/components/views/Home.vue'
import admin from './components/admin/admin.vue'
import test from "../src/components/test"
import test2 from "../src/components/test2"
import uploadevent from "../src/components/uploads/uploadevent.vue"
import events from "../src/components/events/events.vue"
import event from "../src/components/events/event.vue"
import login from "../src/components/reg_login/login_reg.vue"
import Userprofile from "./UserProfile/UserProfile.vue"
import buy from '../src/components/buyAndSell/buy.vue'
import about from '../src/components/views/About.vue'


Vue.use(Router)

export default new Router({
  // mode: 'history',
  // base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/admin',
      name: 'admin/events',
      component: admin
    },
    {
      path:'/test',
      name: 'test',
      component: test
    },
    {
      path:'/test2',
      name: 'test2',
      component: test2
    },
    {
      path:'/uploads',
      name:'uploads',
      component: uploadevent
    },
    {
      path:'/events',
      name:'events',
      component: events
    },
    {
      path:'/event/:eid',
      name:'event',
      component: event
    },
   
    {
      path:'/login',
      name:'login',
      component: login
    },
    {
      path:'/dash',
      name:'dash',
      component: Userprofile
    },
    {
      path:'/buy',
      name:'buy',
      component: buy
    },
    {
      path:'/about',
      name:'about',
      component: about
    },
 
   
   
  ]
})
